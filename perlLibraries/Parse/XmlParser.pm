#!/usr/bin/perl


use Parse::RecDescent;
use Parse::Node;
use Data::Dumper;

$::RD_ERRORS = 1; # Make sure the parser dies when it encounters an error
$::RD_WARN   = 1; # Enable warnings. This will warn on unused rules &c.
$::RD_HINT   = 1; # Give out hints to help fix problems.



sub addNode{
	$node = new Node();
	@parameters = @_;
	$node->setName($parameters[0]);
	if($noRoot==1) {				
		$currentNode = $node;
		$rootNode = $node;
		$name = $currentNode->getName();
		$noRoot=0;
	}
	else {		
		$node->setParent($currentNode);
		$currentNode->addChild($node);
		$currentNode = $node;
		$name = $currentNode->getName();
	}
	return 1;
	
}

sub addText{	
	@parameters = @_;
	$currentNode->setText($parameters[0]);
	$text = $currentNode->getText();
	return 1;
}

sub moveToParent{
	$currentNode = $currentNode->getParent();
	return 1;
}

# Create and compile the source file
$noRoot=1;
$parser = Parse::RecDescent->new(q(


lp : "<"
rp : ">"
slash : "/"
text : /[\\w\\s\\d\\.\\"\\-]+/
openTag : lp text rp 
	{&main::addNode($item{text})}
closeTag : lp slash text rp 
	{&main::moveToParent()}
nodeText: text
	{&main::addText($item{text})}
node : openTag nodeContent closeTag  
nodeContent : nodeText | node(s)
));
1;













