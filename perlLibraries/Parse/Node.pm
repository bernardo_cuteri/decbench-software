#!/usr/bin/perl

package Node;
sub new
{
    my $class = shift;
    my $self = {
        _name => "",
        _text  => "",
 		_parent => "",
		_children => []
		#bless { _child=>""}, $class
    };
    # Print all the values just for clarification.
    bless $self, $class;
    return $self;
}

sub setName {
    my ( $self, $name ) = @_;
    $self->{_name} = $name if defined($name);
    return $self->{_name};
}

sub setText {
     my ( $self, $text ) = @_;
    $self->{_text} = $text if defined($text);
    return $self->{_text};
}

sub addChild {
    my ( $self, $child ) = @_;
	#bless $child, "Node";
	#the error was here
	push @{$self->{_children}}, $child;
}


sub getChildren {
	my( $self ) = @_;
    return @{$self->{_children}};
}

sub setParent {
    my ( $self, $parent ) = @_;
    $self->{_parent} = $parent if defined($parent);
    return $self->{_parent};
}

sub getName {
    my( $self ) = @_;
    return $self->{_name};
}
sub getText {
    my( $self ) = @_;
    return $self->{_text};
}
sub getParent {
    my( $self ) = @_;
    return $self->{_parent};
}
1;
