#!/bin/bash

DLV=dlv
DIRNAME=`dirname $0`
TMPFILE=`mktemp --suffix=.decbench`

$DLV -silent -filter=requires -filter=testcaseCommand -filter=testcaseExecutionData -filter=resourceLimit $DIRNAME/staticPredicates.dl $@ > $TMPFILE
$DIRNAME/genMakefile.pl $TMPFILE > Makefile
rm -f $TMPFILE
