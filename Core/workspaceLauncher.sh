./genDomainEntities.pl -regex="^\\w+-\\w+-(\\d+)" -domain="threateningQueens" -regexUsage="dlv:-N="  -insideRegex="dimension\\((\\d+)\\)" -insideAttribute="dimension" -command="-filter=filled DomainExample/encoding/encoding.dl" -testcasesDir="DomainExample/threateningInstances" > testcases.dl
./addOrderingRule.pl dlv threateningQueens dimension ascending strong testcases.dl
./genMakefile.sh benchmark.dl testcases.dl
make -k -j2
