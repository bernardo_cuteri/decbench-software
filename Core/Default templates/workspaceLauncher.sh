./genDomainEntities.pl -regex="(\\d+)-0.asp" -regexUsage="dlv:-N=" -instancesFolder="../02-Reachability/selectedInstances" -domain="reachability" -insideOccurrences="edge" -command="../02-Reachability/command.asp" > testcases.dl

./genDomainEntities.pl -regex="(\\d+)-0.asp" -regexUsage="dlv:-N=" -instancesFolder="../03-StrategicCompanies/selectedInstances" -domain="strategicCompanies" -insideOccurrences="controlled_by produced_by" -command="../03-StrategicCompanies/command.asp" >> testcases.dl

./genDomainEntities.pl -regex="(\\d+)-0.asp" -regexUsage="dlv:-N=" -instancesFolder="../06-Grammar-BasedInformationExtraction/selectedInstances" -domain="grammarBasedInformationExtraction" -insideOccurrences="char" -command="../06-Grammar-BasedInformationExtraction/command.asp" >> testcases.dl

./genDomainEntities.pl -regex="(\\d+)-0.asp" -regexUsage="dlv:-N=" -instancesFolder="../10-SokobanDecision/selectedInstances" -domain="sokobanDecision" -insideOccurrences="top right next step solution box sokoban" -command="../10-SokobanDecision/command.asp" >> testcases.dl

./genDomainEntities.pl -regex="(\\d+)-0.asp" -regexUsage="dlv:-N=" -instancesFolder="../12-KnightTour/selectedInstances" -domain="knightTour" -insideRegex="size\\((\\d+)\\)" -insideAttribute="size" -command="../12-KnightTour/command.asp" >> testcases.dl

./genDomainEntities.pl -regex="(\\d+)-0.asp" -regexUsage="dlv:-N=" -instancesFolder="../13-DisjunctiveScheduling/selectedInstances" -domain="disjunctiveScheduling" -insideOccurrences="task est let disj" -command="../13-DisjunctiveScheduling/command.asp" >> testcases.dl

./genDomainEntities.pl -regex="(\\d+)-0.asp" -regexUsage="dlv:-N=" -instancesFolder="../14-PackingProblem/selectedInstances" -domain="packingProblem" -insideOccurrences="square" -insideRegex="max_square_num\\((\\d+)\\)" -insideAttribute="maxSquareNum" -command="../14-PackingProblem/command.asp" >> testcases.dl

./genDomainEntities.pl -regex="(\\d+)-0.asp" -regexUsage="dlv:-N=" -instancesFolder="../17-Labyrinth/selectedInstances" -domain="labyrinth" -insideRegex="max_steps\\((\\d+)\\)" -insideAttribute="maxSteps" -insideOccurrences="field connect" -command="../17-Labyrinth/command.asp" >> testcases.dl

./genDomainEntities.pl -regex="(\\d+)-0.asp" -regexUsage="dlv:-N=" -instancesFolder="../18-MinimalDiagnosis/selectedInstances" -domain="minimalDiagnosis" -insideOccurrences="edge input vertex obs_elabel obs_vlabel" -command="../18-MinimalDiagnosis/command.asp" >> testcases.dl

./genDomainEntities.pl -regex="(\\d+)-0.asp" -regexUsage="dlv:-N=" -instancesFolder="../19-MultiContextSystemQuerying/selectedInstances" -domain="multiContextSystemQuerying" -insideOccurrences="ctxrule brule" -command="../19-MultiContextSystemQuerying/command.asp" >> testcases.dl

./genDomainEntities.pl -regex="(\\d+)-0.asp" -regexUsage="dlv:-N=" -instancesFolder="../20-Numberlink/selectedInstances" -domain="numberlink" -insideOccurrences="node edge connection" -command="../20-Numberlink/command.asp" >> testcases.dl

./genDomainEntities.pl -regex="(\\d+)-0.asp" -regexUsage="dlv:-N=" -instancesFolder="../22-HanoiTower/selectedInstances" -domain="hanoiTower" -insideOccurrences="disk time" -command="../22-HanoiTower/command.asp" >> testcases.dl

./genDomainEntities.pl -regex="(\\d+)-0.asp" -regexUsage="dlv:-N=" -instancesFolder="../25-GraphColouring/selectedInstances" -domain="graphColouring" -insideOccurrences="node link" -command="../25-GraphColouring/command.asp" >> testcases.dl

./genDomainEntities.pl -regex="(\\d+)-0.asp" -regexUsage="dlv:-N=" -instancesFolder="../26-Solitaire/selectedInstances" -domain="solitaire" -insideOccurrences="time full" -command="../26-Solitaire/command.asp" >> testcases.dl

./genDomainEntities.pl -regex="(\\d+)-0.asp" -regexUsage="dlv:-N=" -instancesFolder="../28-Weight-AssignmentTree/selectedInstances" -domain="weightAssignmentTree" -insideOccurrences="leafWeightCardinality innerNode" -insideRegex="max_total_weight\\((\\d+)\\)" -insideAttribute="maxTotalWeight" -command="../28-Weight-AssignmentTree/command.asp" >> testcases.dl

./genDomainEntities.pl -regex="(\\d+)-0.asp" -regexUsage="dlv:-N=" -instancesFolder="../30-HydraulicLeaking/selectedInstances" -domain="hydraulicLeaking" -insideOccurrences="tank jet junction valve link full goal" -command="../30-HydraulicLeaking/command.asp" >> testcases.dl

./genDomainEntities.pl -regex="(\\d+)-0.asp" -regexUsage="dlv:-N=" -instancesFolder="../31-HydraulicPlanning/selectedInstances" -domain="hydraulicPlanning" -insideOccurrences="tank jet junction valve link full goal" -command="../31-HydraulicPlanning/command.asp" >> testcases.dl

./genDomainEntities.pl -regex="(\\d+)-0.asp" -regexUsage="dlv:-N=" -instancesFolder="../32-StableMarriage/selectedInstances" -domain="stableMarriage" -insideOccurrences="manAssignsScore" -command="../32-StableMarriage/command.asp" >> testcases.dl

./genDomainEntities.pl -regex="(\\d+)-0.asp" -regexUsage="dlv:-N=" -instancesFolder="../33-MazeGeneration/selectedInstances" -domain="mazeGeneration" insideRegex="maxRow\\((\\d+)\\)" -insideAttribute="maxRow" -insideOccurrences="input_empty input_wall" -command="../33-MazeGeneration/command.asp" >> testcases.dl

./genDomainEntities.pl -regex="(\\d+)-0.asp" -regexUsage="dlv:-N=" -instancesFolder="../34-PartnerUnitsPolynomial/selectedInstances" -domain="partnerUnitsPolynomial" -insideOccurrences="zone2sensor comUnit" -command="../34-PartnerUnitsPolynomial/command.asp" >> testcases.dl

./staticCalls.sh testcases.dl
