set terminal png
set output "cactus_threateningQueens.png"
set xlabel "Solved Instances"
set ylabel "Time (s)"
plot "dlv.dat" using 2:1 w lp
